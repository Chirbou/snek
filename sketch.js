const HEIGHT = 20
const WIDTH = 30
const TILE_SIZE = 30
const SPEED_GAME = 10

function setup() {
  createCanvas(TILE_SIZE * WIDTH, TILE_SIZE * HEIGHT);
}

let frame = 0

function draw() {
  background(0);
  frameRate(60);
  if (frame % parseInt(100 / SPEED_GAME) === 0) {
    snek.move();
    snek.clong()
    snek.hit(pom.x, pom.y)
    snek.canibalism()
  }
  snek.draw();
  pom.draw();
  frame++
}

window.addEventListener('keydown', function (e) {
  if (e.keyCode === 37 && snek.facing !== 'right') {
    e.preventDefault()
    snek.facingWanted = "left";
  }
  if (e.keyCode === 38 && snek.facing !== 'down') {
    e.preventDefault()
    snek.facingWanted = "up";
  }
  if (e.keyCode === 39 && snek.facing !== 'left') {
    e.preventDefault()
    snek.facingWanted = "right";
  }
  if (e.keyCode === 40 && snek.facing !== 'up') {
    e.preventDefault()
    snek.facingWanted = "down";
  }
})

class Block {
  constructor(x, y, color = 255) {
    this.x = x
    this.y = y
    this.color = color
    this.width = TILE_SIZE
    this.height = TILE_SIZE
    this.full = false
  }

  draw(color) {
    fill(color || this.color)
    if (this.full) {
      rect(this.x * this.width - 2, this.y * this.height - 2, this.width + 4, this.height + 4)
      fill('#FF8888')
      rect(this.x * this.width + this.width / 4, this.y * this.height + this.height / 4, this.width / 2, this.height / 2)
    } else {
      rect(this.x * this.width, this.y * this.height, this.width, this.height)
    }
  }

  move(facing) {
    if (facing === 'right') {
      this.x++;
    }
    if (facing === 'up') {
      this.y--;
    }
    if (facing === 'down') {
      this.y++;
    }
    if (facing === 'left') {
      this.x--;
    }
  }
}

class Pom extends Block {
  constructor(color = '#FF0000') {
    let x = parseInt(Math.random() * (WIDTH - 10))
    let y = parseInt(Math.random() * (HEIGHT - 10))
    super(x, y, color)
  }
}

class Snek {
  constructor() {
    this.head = new Block(1, 1)
    this.queue = []
    this.facing = 'right'
    this.facingWanted = 'right'
    this.color = '#00FF00'
  }

  hit(x, y) {
    if (this.head.x === x && this.head.y === y) {
      pom = new Pom();
      this.head.full = true
    }
  }

  draw() {
    this.head.draw(this.color)
    this.queue.forEach(block => {
      block.draw()
    })
  }

  clong() {
    if (this.head.x < 0 || this.head.x >= WIDTH || this.head.y < 0 || this.head.y >= HEIGHT) {
      window.location.reload()
    }
  }

  canibalism() {
    for (let i in this.queue) {
      let block = this.queue[i]
      if (block.x === this.head.x && block.y === this.head.y) {
        window.location.reload()
      }
    }
  }

  move() {
    this.queue.unshift(new Block(this.head.x, this.head.y))
    this.queue[0].full = this.head.full
    this.head.full = false

    if (this.queue[this.queue.length - 1].full) {
      this.queue[this.queue.length - 1].full = false
    }
    else {
      this.queue.pop();
    }

    this.facing = this.facingWanted
    this.head.move(this.facing)
  }
}

let snek = new Snek();
let pom = new Pom();
